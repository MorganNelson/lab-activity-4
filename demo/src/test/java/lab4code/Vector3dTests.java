//youry Nelson
package lab4code;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import lab4code.Vector3d;

public class Vector3dTests {

    @Test
    public void testGetters() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(1, vector.getX(), 0.0001);
        assertEquals(2, vector.getY(), 0.0001);
        assertEquals(3, vector.getZ(), 0.0001);
    }

    @Test
    public void testMagnitude() {
        Vector3d vector = new Vector3d(3, 4, 5);
        assertEquals(7.0711, vector.magnitude(), 0.0001);
    }

    @Test
    public void testDotProduct() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);
        assertEquals(32, vector1.dotProduct(vector2), 0.0001);
    }

    @Test
    public void testAdd() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);
        Vector3d sum = vector1.add(vector2);
        assertEquals(5, sum.getX(), 0.0001);
        assertEquals(7, sum.getY(), 0.0001);
        assertEquals(9, sum.getZ(), 0.0001);
    }
}

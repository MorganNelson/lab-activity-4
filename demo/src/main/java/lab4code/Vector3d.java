//Youry Nelson
package lab4code;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    // Constructor
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // Getter methods
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double magnitude() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public double dotProduct(Vector3d other) {
        return x * other.x + y * other.y + z * other.z;
    }

    public Vector3d add(Vector3d other) {
        double newX = this.x + other.x;
        double newY = this.y + other.y;
        double newZ = this.z + other.z;
        return new Vector3d(newX, newY, newZ);
    }
}